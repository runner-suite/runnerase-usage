{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"side-menu-class-name\">SymmetryFunctionSet</code> and <code class=\"side-menu-class-name\">SymmetryFunction</code> classes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Run this notebook directly in your browser through Binder:\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/runner-suite%2Frunnerase-usage-environment/main?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.com%252Frunner-suite%252Frunnerase-usage%26urlpath%3Dtree%252Frunnerase-usage%252Fsymfuns_classes.ipynb%26branch%3Dmain)_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creating, storing and managing symmetry functions is an important task on the road to a well-functioning HDNNP. runnerase provides two separate classes for dealing with symmetry functions: the `SymmetryFunction` and `SymmetryFunctionSet` objects.\n",
    "\n",
    "<div class=\"admonition tip\">\n",
    "    <p class=\"admonition-title\">Target audience</p>\n",
    "    <p>\n",
    "        This notebook is targeted towards advanced users with special symmetry function needs. Most peoples needs will already be satisfied by the `generate_symmetryfunctions` routine that is also part of runnerase. It automatically generates whole sets of symmetry functions based on your dataset. Head over to the corresponding notebook to learn more.\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Initializing a `SymmetryFunction` object"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "from runnerase.symmetry_functions import SymmetryFunction, SymmetryFunctionSet"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Creating a single symmetry function is as easy as calling"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "SymmetryFunction(sftype=None, cutoff=None, elements=None, coefficients=None)"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sf = SymmetryFunction()\n",
    "sf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When the object is initialized like this, without any parameters, the symmetry function is completely untyped. We can create a full radial symmetry function by changing these properties one by one: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.1, 0.0])"
      ]
     },
     "execution_count": 15,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sf.sftype = 2\n",
    "sf.cutoff = 10.0\n",
    "sf.elements = ['Cu', 'Zn']\n",
    "sf.coefficients = [0.1, 0.0]\n",
    "sf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should be careful: the `SymmetryFunction` API is very raw and does not provide any type checks for the individual parameters. Therefore, you can easily create a symmetry function that cannot be written correctly to an input.nn file.\n",
    "\n",
    "The arguments can also be supplied to the symmetry function during object instantialization:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.1, 0.0])"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sf = SymmetryFunction(\n",
    "    sftype=2,\n",
    "    cutoff=10.0,\n",
    "    elements=['Cu', 'Zn'],\n",
    "    coefficients=[0.1, 0.0]\n",
    ")\n",
    "sf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, the two objects do not differ at all.\n",
    "\n",
    "The third way to create a single symmetry function is to supply the parameters in list form like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.1, 0.0])"
      ]
     },
     "execution_count": 17,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sf = SymmetryFunction(sflist=['Cu', 2, 'Zn', 0.1, 0.0, 10.0])\n",
    "sf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is the way that is used internally by runnerase to pass symmetry functions arouns. However, it is very easy to introduce ordering mistakes in here. Beware!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functionalities and Properties of the `SymmetryFunction` object"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Any `SymmetryFunction` can tell you whether it is radial or angular by calling its `tag`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "'radial'"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "sf.tag"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Imagine you just created one symmetry function and want to create a second one with a different central atom element. In that case, you `copy` the first symmetry function to a new object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "New copy:  SymmetryFunction(sftype=2, cutoff=10.0, elements=['Zr', 'Zn'], coefficients=[0.1, 0.0])\n",
      "     Old:  SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.1, 0.0])\n"
     ]
    }
   ],
   "source": [
    "sf = SymmetryFunction(sflist=['Cu', 2, 'Zn', 0.1, 0.0, 10.0])\n",
    "\n",
    "new_sf = sf.copy()\n",
    "new_sf.elements[0] = 'Zr'\n",
    "print('New copy: ', new_sf)\n",
    "print('     Old: ', sf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This feature becomes especially interesting when you see that, to a certain degree, symmetry functions behave like mathematical objects. You can add them up and multiply them.\n",
    "\n",
    "Adding two `SymmetryFunction`s will create a new object called a `SymmetryFunctionSet`. We will go into more detail below. It is basically a container to hold multiple symmetry functions at once."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SymmetryFunctionSet(type=radial, sets=0, symmetryfunctions=2)\n"
     ]
    }
   ],
   "source": [
    "sf = SymmetryFunction(sflist=['Cu', 2, 'Zn', 0.1, 0.0, 10.0])\n",
    "\n",
    "sf += sf\n",
    "print(sf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Multiplication works in a similar way. Watch how we create a `SymmetryFunctionSet` with 8 individual symmetry functions and then build up their `eta` parameters in a quick loop:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.0, 0.0])\n",
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.1, 0.0])\n",
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.2, 0.0])\n",
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.30000000000000004, 0.0])\n",
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.4, 0.0])\n",
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.5, 0.0])\n",
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.6000000000000001, 0.0])\n",
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.7000000000000001, 0.0])\n"
     ]
    }
   ],
   "source": [
    "sf = SymmetryFunction(sflist=['Cu', 2, 'Zn', 0.1, 0.0, 10.0])\n",
    "\n",
    "sfset = sf * 8\n",
    "\n",
    "for idx, i in enumerate(sfset):\n",
    "    i.coefficients = [0.1 * idx, 0.0]\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With these two functionalities, you can build up complex patterns of symmetry functions in a simple way. Feel free to experiment!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Internally, multiplication uses `copy` so you do not have to worry about overwriting things."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can also convert `SymmetryFunction`s to different formats. `todict` and `to_list` will print a dictionary or a list, respectively. `to_runner` will print an \"input.nn\"-compatible string representation of the symmetry function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cu 2 Zn      0.10000000       0.00000000      10.00000000\n",
      "{'sftype': 2, 'cutoff': 10.0, 'elements': ['Cu', 'Zn'], 'coefficients': [0.1, 0.0]}\n",
      "('Cu', 2, 'Zn', 0.1, 0.0, 10.0)\n"
     ]
    }
   ],
   "source": [
    "print(sf.to_runner())\n",
    "print(sf.todict())\n",
    "print(sf.to_list())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The `SymmetryFunctionSet` class\n",
    "\n",
    "As promised, there is more to say about the `SymmetryFunctionSet` object. It is a recursive object, meaning that it may contain more `SymmetryFunctionSet`s. For example, an outer set can contain one set of radial and one set of angular symmetry functions.\n",
    "\n",
    "Let us create a first object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SymmetryFunctionSet(type=mixed, sets=0, symmetryfunctions=2)\n"
     ]
    }
   ],
   "source": [
    "sfset = SymmetryFunctionSet(sflist=[['Cu', 2, 'Zn', 0.1, 0.0, 10.0], ['Cu', 3, 'Zn', 'Zn', 0.0, 1.0, 0.3, 10.0]])\n",
    "print(sfset)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We instantialized the object from a list of two symmetry functions, one radial, one angular. Therefore, the string representation of the set tells us that is has a `mixed` type and holds two symmetry functions. At this point, it does not contain any other `SymmetryFunctionSet`s.\n",
    "\n",
    "<div class=\"admonition tip\">\n",
    "    <p class=\"admonition-title\">`__str__` vs `__repr__`</p>\n",
    "    <p>\n",
    "        There is a difference between calling `print(sfset)` or simply putting `sfset` at the end of a cell. One will print a clean summary of the object, the other shows a list representation of all contained symmetry functions. This is because these two ways of printing access different special methods of the object: `__str__` and `__repr__`.\n",
    "    </p>\n",
    "</div>\n",
    "\n",
    "We can easily create a set containing two sets at once:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SymmetryFunctionSet(type=None, sets=2, symmetryfunctions=0)\n"
     ]
    }
   ],
   "source": [
    "radials = SymmetryFunctionSet(sflist=[['Cu', 2, 'Zn', 0.1, 0.0, 10.0], ['Cu', 2, 'Cu', 0.1, 0.0, 10.0]])\n",
    "angulars = SymmetryFunctionSet(sflist=[['Cu', 3, 'Cu', 'Zn', 0.0, 1.0, 0.3, 12.0], ['Cu', 3, 'Zn', 'Zn', 0.0, 1.0, 0.3, 10.0]])\n",
    "\n",
    "sfset = SymmetryFunctionSet()\n",
    "sfset.append(radials)\n",
    "sfset.append(angulars)\n",
    "\n",
    "print(sfset)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can look at the two sets by accessing the `sets` property:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SymmetryFunctionSet(type=radial, sets=0, symmetryfunctions=2)\n",
      "SymmetryFunctionSet(type=angular, sets=0, symmetryfunctions=2)\n"
     ]
    }
   ],
   "source": [
    "for set in sfset.sets:\n",
    "    print(set)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will find that the outer `sfset` object does not contain any symmetry functions, while the inner two sets contain two symmetry functions each, but no further sets.\n",
    "\n",
    "Moreover, the `SymmetryFunctionSet` property `storage` allows you to recursively join all symmetry functions together, no matter if they are contained in another set or directly attached to the set itself: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.1, 0.0])\n",
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Cu'], coefficients=[0.1, 0.0])\n",
      "SymmetryFunction(sftype=3, cutoff=12.0, elements=['Cu', 'Cu', 'Zn'], coefficients=[0.0, 1.0, 0.3])\n",
      "SymmetryFunction(sftype=3, cutoff=10.0, elements=['Cu', 'Zn', 'Zn'], coefficients=[0.0, 1.0, 0.3])\n"
     ]
    }
   ],
   "source": [
    "for sf in sfset.storage:\n",
    "    print(sf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Math with `SymmetryFunctionSet`s\n",
    "\n",
    "Similar to the `SymmetryFunction`s, `SymmetryFunctionSet`s also have the ability to be summed (but not multiplied). This makes it very to add new symmetry functions to an existing storage:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "SymmetryFunctionSet(type=mixed, sets=0, symmetryfunctions=4)\n",
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.1, 0.0])\n",
      "SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Cu'], coefficients=[0.1, 0.0])\n",
      "SymmetryFunction(sftype=3, cutoff=12.0, elements=['Cu', 'Cu', 'Zn'], coefficients=[0.0, 1.0, 0.3])\n",
      "SymmetryFunction(sftype=3, cutoff=10.0, elements=['Cu', 'Zn', 'Zn'], coefficients=[0.0, 1.0, 0.3])\n"
     ]
    }
   ],
   "source": [
    "radials = SymmetryFunctionSet(sflist=[['Cu', 2, 'Zn', 0.1, 0.0, 10.0], ['Cu', 2, 'Cu', 0.1, 0.0, 10.0]])\n",
    "angulars = SymmetryFunctionSet(sflist=[['Cu', 3, 'Cu', 'Zn', 0.0, 1.0, 0.3, 12.0], ['Cu', 3, 'Zn', 'Zn', 0.0, 1.0, 0.3, 10.0]])\n",
    "\n",
    "sfset = SymmetryFunctionSet()\n",
    "sfset += radials\n",
    "sfset += angulars\n",
    "\n",
    "print(sfset)\n",
    "for sf in sfset.storage:\n",
    "    print(sf)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Adding two `SymmetryFunctionSet`s works a little differenly than calling `append`. The line `sfset += radials` will append all `SymmetryFunctionSet`s in `radials` to `sfset` **and also** append all `SymmetryFunction`s in `radials` to `sfset`. In other words, `+` takes a `SymmetryFunctionSet` apart and puts its pieces into the right locations in a new `SymmetryFunctionSet`.\n",
    "\n",
    "In contrast to that, when you `append` you just add the whole new `SymmetryFunctionSet` to the storage of the other one as it is."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Further properties of `SymmetryFunctionSet`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similar to `SymmetryFunctions`, you can convert sets to lists and dictionaries:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[('Cu', 2, 'Zn', 0.1, 0.0, 10.0), ('Cu', 2, 'Cu', 0.1, 0.0, 10.0), ('Cu', 3, 'Cu', 'Zn', 0.0, 1.0, 0.3, 12.0), ('Cu', 3, 'Zn', 'Zn', 0.0, 1.0, 0.3, 10.0)]\n",
      "{'sets': [], 'symmetryfunctions': [SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Zn'], coefficients=[0.1, 0.0]), SymmetryFunction(sftype=2, cutoff=10.0, elements=['Cu', 'Cu'], coefficients=[0.1, 0.0]), SymmetryFunction(sftype=3, cutoff=12.0, elements=['Cu', 'Cu', 'Zn'], coefficients=[0.0, 1.0, 0.3]), SymmetryFunction(sftype=3, cutoff=10.0, elements=['Cu', 'Zn', 'Zn'], coefficients=[0.0, 1.0, 0.3])], 'min_distances': None}\n"
     ]
    }
   ],
   "source": [
    "print(sfset.to_list())\n",
    "\n",
    "print(sfset.todict())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A `SymmetryFunctionSet` also carries several shorthands to inform you about the properties of the symmetry functions it contains:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      " Cutoffs:  [10.0, 12.0]\n",
      "Elements:  ['Cu', 'Zn']\n",
      "   Types:  mixed\n"
     ]
    }
   ],
   "source": [
    "print(' Cutoffs: ', sfset.cutoffs)\n",
    "print('Elements: ', sfset.elements)\n",
    "print('   Types: ', sfset.sftypes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"admonition warning\">\n",
    "    <p class=\"admonition-title\">Fair warning</p>\n",
    "    <p>\n",
    "        `cutoffs`, `elements` and `sftypes` only prints information about the data contained in `sfset.symmetryfunctions`, but does not tell you anything about the symmetry functions in other `sfsets.sets`. Put differently, they are non-recursive.\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A set can easily inform you about the number of symmetry functions it contains:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "4"
      ]
     },
     "execution_count": 42,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "len(sfset)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, if you wish to remove all data from a container you can call `reset` to put the `SymmetryFunctionSet` into a clean state. This comes in especially handy in Jupyter notebooks, where people tend to execute cells multiple times and end up with the same symmetry functions multiple times."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Plotting symmetry functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, `SymmetryFunctionSet` objects also come with a handy `plot`ting interface. Some nice examples of this are shown in [Generating symmetry functions](../symfuns_generation)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "runnerase-demo",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
