{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# MD with runnerase and LAMMPS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Run this notebook directly in your browser through Binder:\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/runner-suite%2Frunnerase-usage-environment/main?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.com%252Frunner-suite%252Frunnerase-usage%26urlpath%3Dtree%252Frunnerase-usage%252Fapplication_mdlammps.ipynb%26branch%3Dmain)_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This example demonstrates how to run a simulation using LAMMPS and runnerase."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "\n",
    "import numpy as np\n",
    "\n",
    "from ase.io import read\n",
    "\n",
    "from runnerase.calculators.runner import Runner\n",
    "from runnerase.workflows import run_nvt_simulation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We initialize a calculator from a previous calculation. Here, it is very important to specify the `label` argument, because LAMMPS will look for files like the weights or input.nn under the path given by `label`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "RUNNER_CMD = 'runner1 > PREFIX.out'\n",
    "calc = Runner(\n",
    "    command=RUNNER_CMD,\n",
    "    restart='data/mode3/mode3',\n",
    "    label='data/mode3/mode3'\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also, we prepare the initial structure for the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "atoms = calc.dataset[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running an NVT simulation\n",
    "\n",
    "runnerase comes with a workflow that also you to run an NVT simulation. This workflow has a certain amount of variability, but it probably is not a one-suits-all solution. Instead, it is meant as an example of how to run LAMMPS with runnerase and should be fairly straighforward to extend to anyones personal needs. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to use `run_nvt_simulation`, we create a random number generator (for initializing velocities) and supply the command with our personal simulation settings. Here, we list almost all possible settings even though many of them are the default anyway."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "rng = np.random.default_rng(42)\n",
    "\n",
    "extrapolations = run_nvt_simulation(\n",
    "    calc=calc,\n",
    "    initial_structure=atoms,\n",
    "    directory='simulation',\n",
    "    rng=rng,\n",
    "    output_freq=1,\n",
    "    thermo_style='custom step temp pe etotal vol pxx',\n",
    "    temp=300,\n",
    "    thermo_damping=100,\n",
    "    num_steps=1000,\n",
    "    timestep=5e-4\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most parameters are pretty self-explanatory. `output_freq` will control how often both the trajectory and the thermo data is written. `temp` can be either a single temperature or a range (the latter will apply a temperature ramp). The `timestep` is given in metal units by default. \n",
    "\n",
    "<div class=\"admonition danger\">\n",
    "    <p class=\"admonition-title\">Typical error</p>\n",
    "    <p>\n",
    "        If no \"elements\" argument is supplied to the function, runnerase will resort to the elements present in the training dataset. Please note that if your simulated system contains less atoms than are present in your training dataset, this will fail with an error for the pair_coeff setup!\n",
    "    </p>\n",
    "</div>\n",
    "\n",
    "The simulation returns a `RunnerExtrapolationWarnings` object which contains all extrapolation warning that occured during the simulation. More on this later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can render the output of the simulation using OVITO and display the GIF directly in the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ovito.vis import TachyonRenderer, Viewport\n",
    "from runnerase.visualize.ovito import render\n",
    "from ase.io import read\n",
    "\n",
    "renderer = TachyonRenderer(\n",
    "    antialiasing_samples = 5, \n",
    "    direct_light_intensity = 0.8, \n",
    "    ambient_occlusion_brightness = 0.6\n",
    ")\n",
    "\n",
    "# Viewport setup:\n",
    "viewport = Viewport(\n",
    "    type = Viewport.Type.Ortho, \n",
    "    fov = 7.11327978881, \n",
    "    camera_dir = (-0.2853971364811249, 0.9410986393930905, -0.18133346470204595), \n",
    "    camera_pos = (3.8121338588326226, 3.4250716615686523, 3.8164381878002414)\n",
    ")\n",
    "\n",
    "render(\n",
    "    read('simulation/dump.lammpstrj', index=':'),\n",
    "    'resources/lammpstraj.gif',\n",
    "    renderer=renderer,\n",
    "    viewport=viewport,\n",
    "    image_size=(600, 600)\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"../resources/lammpstraj.gif\" width=\"750\" align=\"center\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The trajectory does not show contain any element information anymore because these are not part of a LAMMPS file dump."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Digging deeper: how it works"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "from ase.calculators.lammpslib import LAMMPSlib\n",
    "\n",
    "from runnerase.storageclasses import RunnerExtrapolationWarnings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For instructional reasons, let us take apart the key steps of the `run_nvt_simulation` routine.\n",
    "\n",
    "First, it defines some necessary paths and creates the calculation directory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "directory = 'simulation'\n",
    "logfile = os.path.join(directory, 'lammps.log')\n",
    "dumpfile = os.path.join(directory, 'dump.lammpstrj')\n",
    "initialfile = os.path.join(directory, 'initial.data')\n",
    "\n",
    "if not os.path.exists(directory):\n",
    "    os.makedirs(directory)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define the calculation parameters by hand here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "temp = [300.0, 2000.0]\n",
    "timestep = 5e-4\n",
    "num_steps = 10000\n",
    "rng = np.random.default_rng(42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now come the crucial parts: we call the `get_lammps_potential` routine of the `Runner` calculator, which returns the pair_coeff and pair_style keywords for LAMMPS."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['pair_style hdnnp 6.3501265033677905 dir \"data/mode3\" showew yes showewsum 0 resetew no maxew 100 cflength 1.8897261328 cfenergy 0.0367493254',\n",
       " 'pair_coeff * * Li Al']"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Create a LAMMPS calculation based on the RuNNer fit.\n",
    "cmds = calc.get_lammps_potential()\n",
    "cmds"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using these commands, we initialize the `LAMMPSLib` calculator provided by ASE (which in turns talks to LAMMPS via their Python interfaces)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create and initialize a LAMMPS calculator.\n",
    "lmp = LAMMPSlib(\n",
    "    lmpcmds=cmds,\n",
    "    log_file=logfile,\n",
    "    keep_alive=False\n",
    ")\n",
    "lmp.start_lammps()\n",
    "lmp.initialise_lammps(atoms)\n",
    "\n",
    "# Convert the box dimensions.\n",
    "lmp.lmp.command(f'change_box all boundary {lmp.lammpsbc(atoms)}')\n",
    "lmp.set_lammps_pos(atoms)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After the setup of the structure is done, we write it to a file:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lmp.lmp.command(f'write_data {initialfile}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We tell LAMMPS how to write thermo and trajectory information"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lmp.lmp.command(f'thermo 10')\n",
    "lmp.lmp.command(f'thermo_style custom step temp pe etotal vol pxx')\n",
    "lmp.lmp.command(f'dump 1 all atom 10 {dumpfile}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we create the thermostat, the initial velocity distribution and define the timestep."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lmp.lmp.command(\n",
    "    f'fix 1 all nvt temp {temp[0]} {temp[1]} 100'\n",
    ")\n",
    "lmp.lmp.command(\n",
    "    f'velocity all create 300 {int(rng.integers(1, 100000))}'\n",
    ")\n",
    "\n",
    "lmp.lmp.command(f'timestep {timestep}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All that is left is to run the simulation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lmp.lmp.command(f'run {num_steps}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we parse the extrapolation warnings into the `RunnerExtrapolationWarnings` object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "extrapolations = RunnerExtrapolationWarnings(logfile)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"admonition tip\">\n",
    "    <p class=\"admonition-title\">Queue submission</p>\n",
    "    <p>\n",
    "        Of course, you can also use the submit_slurm routine to submit such a calculation to the queue.\n",
    "    </p>\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
