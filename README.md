# runnerase Usage Examples

This repository contains Jupyter notebooks that demonstrate how to use runnerase. The contents are rendered and displayed as part of the [runnerase documentation](https://runner-suite.gitlab.io/runnerase/latest/).

## Questions, Contributions, Suggestions

Everyone is warmly invited to open issues and merge requests here on Gitlab
or [contact the developer](mailto:alexander.knoll@rub.de).

## Credits

The code is written and maintained by Alexander Knoll - @aknoll - [alexknoll@mailbox.org](mailto:alexander.knoll@rub.de).

## License

This software is distributed under the GPLv3. For details, see [LICENSE](LICENSE).
