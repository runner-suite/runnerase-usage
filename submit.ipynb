{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Submitting calculations to the queue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `runnerase.utils.submit` module provides two very valuable functions to submit\n",
    "calculations to the SLURM queuing system: `submit_slurm` and `slurm_barrier`\n",
    "\n",
    "<div class=\"admonition info\">\n",
    "    <p class=\"admonition-title\">Other scheduling systems</p>\n",
    "    <p>\n",
    "        Other schedulers might work but have not been tested\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`submit_slurm` wraps any other function and makes it queue-submittable.\n",
    "It achieves this in key three steps:\n",
    "\n",
    "1. Wrap the function and its arguments in a dictionary and pickle this\n",
    "    dict to a file.\n",
    "2. Write a python script that can load and execute the function from\n",
    "    the pickle file.\n",
    "3. Write a bash script than executes the Python function\n",
    "4. Submit the job with `sbatch`.\n",
    "\n",
    "`slurm_barrier` hangs until a given job has vanished from the queue. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from runnerase.calculators.runner import Runner\n",
    "from runnerase import generate_symmetryfunctions\n",
    "from ase.io import read\n",
    "from runnerase.workflows import train_hdnnp\n",
    "from runnerase.utils import submit_slurm, slurm_barrier"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before `submit_slurm` can work, the user has to do some configuration. As explained above, `submit_slurm` will write a Bash script that submits a Python script to the queue. The template for this Bash script is specific to each queue and has to be provided by the user. \n",
    "\n",
    "runnerase searches for this script in two locations:\n",
    "* Within a directory called `$HOME/.runnerase/`\n",
    "* In the folder were the script lies from which `submit_slurm` is called.\n",
    "\n",
    "The filename must follow the naming scheme \"queue_name.sh\" where queue_name can be anything you want. This name is passed to `submit_slurm` as a required parameter.\n",
    "\n",
    "Here is an example of how that file might look:\n",
    "\n",
    "```sh\n",
    "#!/bin/bash\n",
    "#SBATCH --time={{runtime_max}}\n",
    "#SBATCH --mem={{memory_max}}\n",
    "#SBATCH --cpus-per-task={{num_cores}}\n",
    "#SBATCH --output={{output_name}}\n",
    "#SBATCH --job-name={{job_name}}\n",
    "#SBATCH --chdir={{working_directory}}\n",
    "#SBATCH --partition={{queue_name}}\n",
    "\n",
    "export OMP_NUM_THREADS=8\n",
    "ulimit -s unlimited\n",
    "\n",
    "module load intel/ifort/2022 intel/mkl/2022 mpi/openmpi-x86_64\n",
    "conda activate runnerase\n",
    "\n",
    "{{command}}\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This file contains placeholders in the format \"{{key}}\". When `submit_slurm` is called, these placeholders are replaced with the appropriate values from the kwargs of the function. `submit_slurm` understand the following keys (defaults are shown, too):\n",
    "\n",
    "```python\n",
    "    'job_name': 'runnerase',\n",
    "    'output_name': 'slurm-%j.out',\n",
    "    'num_cores': None,\n",
    "    'memory_max': '4G',\n",
    "    'queue_name': None,\n",
    "    'runtime_max': '01-00:00:00',\n",
    "```\n",
    "\n",
    "Please note that `queue_name` and `num_cores` are mandatory queues that always have to be specified."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"admonition warning\">\n",
    "    <p class=\"admonition-title\">Slurm output</p>\n",
    "    <p>\n",
    "        It is advisable to set the `output_name` key of `submit_slurm` to a value including a separate directory. This helps keep your file structure clean. However, be warned that calculations with SLURM will silently die without any error if the directory you specified does not exist.\n",
    "    </p>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Submitting a single job"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us see two functions in action. We set up a general `Runner` calculator for training a potential:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = read('data/input.data', index=':', format='runnerdata')\n",
    "\n",
    "RUNNER_CMD = 'runner1 > PREFIX.out'\n",
    "calc = Runner(command=RUNNER_CMD, dataset=dataset)\n",
    "\n",
    "calc.parameters.symfunction_short.reset()\n",
    "\n",
    "radials = generate_symmetryfunctions(\n",
    "    dataset,\n",
    "    sftype=2,\n",
    "    algorithm='turn',\n",
    "    amount=6,\n",
    "    cutoff=12.0\n",
    ")\n",
    "\n",
    "calc.parameters.symfunction_short += radials\n",
    "\n",
    "calc.label = 'fit_1/mode1/mode1'\n",
    "calc.profile.prefix = calc.prefix"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Normally, we would call `calc.run(mode=1)` to start the calculation. Now, we wrap that call into `submit_slurm` and pass `mode=1` in as a kwarg.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "mode1_jobid = submit_slurm(\n",
    "    calc.run,\n",
    "    mode=1,\n",
    "    queue_name='q8',\n",
    "    num_cores=8\n",
    ")\n",
    "\n",
    "slurm_barrier(mode1_jobid)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The job returned successfully from the queue, but the results have not been read. This is because the job ran remotely on a different machine and the final calculator is not sent back to the original process:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{}"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "calc.results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thankfully, we can simply restart the calculator from the files that were written:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'sfvalues': RunnerSymmetryFunctionValues(n_structures=100),\n",
       " 'splittraintest': RunnerSplitTrainTest(n_train=89, n_test=11)}"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "calc = Runner(restart='fit_1/mode1/mode1', command=RUNNER_CMD)\n",
    "calc.results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Chaining things together"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this way, we can build up complex workflows in which the program submits several calculations, waits for their return and then continues along the road. In this fashion, let us continue to train the full potential:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "calc.label = 'fit_1/mode2/mode2'\n",
    "calc.profile.prefix = calc.prefix\n",
    "calc.set(use_short_forces=False)\n",
    "calc.set(epochs=5)\n",
    "\n",
    "jobid = submit_slurm(\n",
    "    calc.run,\n",
    "    mode=2,\n",
    "    queue_name='q8',\n",
    "    num_cores=8\n",
    ")\n",
    "\n",
    "slurm_barrier(jobid)\n",
    "\n",
    "calc = Runner(\n",
    "    restart='fit_1/mode2/mode2',\n",
    "    label='fit_1/mode3/mode3',\n",
    "    command=RUNNER_CMD\n",
    ")\n",
    "\n",
    "jobid = submit_slurm(\n",
    "    calc.run,\n",
    "    mode=3,\n",
    "    queue_name='q8',\n",
    "    num_cores=8\n",
    ")\n",
    "\n",
    "slurm_barrier(jobid)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The results show that the potential was trained successfully."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "dict_keys(['energy', 'forces', 'stress', 'weights', 'scaling'])"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "calc = Runner(restart='fit_1/mode3/mode3', command=RUNNER_CMD)\n",
    "calc.results.keys()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Submit a full `train` workflow\n",
    "\n",
    "As stated above, the `submit_slurm` function is universally applicable. You can use it to run LAMMPS-based simulations with runnerase as well as submit a whole training workflow to the queue. The latter is shown below for the same example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "157526"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "calc = Runner(command=RUNNER_CMD, dataset=dataset)\n",
    "calc.label = 'fit_2/mode1/mode1'\n",
    "calc.profile.prefix = calc.prefix\n",
    "\n",
    "settings = {'mode2': {'epochs': 5}}\n",
    "\n",
    "submit_slurm(\n",
    "    train_hdnnp,\n",
    "    calc,\n",
    "    settings,\n",
    "    queue_name='q8',\n",
    "    num_cores=8\n",
    ")\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "dict_keys(['energy', 'forces', 'stress', 'weights', 'scaling'])"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "calc = Runner(restart='fit_1/mode3/mode3', command=RUNNER_CMD)\n",
    "calc.results.keys()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "runnerase-demo",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
