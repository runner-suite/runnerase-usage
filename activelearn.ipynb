{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The <code class=\"side-menu-class-name\">RunnerActiveLearn</code> class"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "_Run this notebook directly in your browser through Binder:\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/runner-suite%2Frunnerase-usage-environment/main?urlpath=git-pull%3Frepo%3Dhttps%253A%252F%252Fgitlab.com%252Frunner-suite%252Frunnerase-usage%26urlpath%3Dtree%252Frunnerase-usage%252Factivelearn.ipynb%26branch%3Dmain)_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Active Learning is one of the most crucial steps when constructing a HDNNP. runnerase implements a whole `RunnerActiveLearn` calculator to make this task easier for you.\n",
    "\n",
    "In principle, active learning with the `RunnerActiveLearn` calculator consists of four steps:\n",
    "1. **Potential training**: based on the training setups you specify, the calculator will train multiple independent potentials on the same dataset.\n",
    "2. **MD Simulation**: based on multiple simulation setups you provide, the calculator runs simulations for all potentials and records the extrapolation warnings.\n",
    "3. **MD Recalculation**: every simulation is recalculated with each potential that was created in step 1. For example, if you trained 3 potentials and ran 5 simulation setups (= 15 simulations), 45 recalculations will be performed.\n",
    "4. **Analysis**: the calculator will analyze interpolations and extrapolations and recommend structures which should be included in the next training cycle.\n",
    "\n",
    "These four steps are referred to as one active learning cycle. In practice, of course, you still need to recalculate the structures you chose with your reference method and create a new training dataset before the next active learning cycle can start. ASE can also help automate that task, but at this moment this is not implemented in `RunnerActiveLearn`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## General Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "\n",
    "from ase.io import read\n",
    "\n",
    "from runnerase.active_learning import RunnerActiveLearn"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We begin by loading a training dataset for our potentials. We also create a random number generator so that our runs are reproducible."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "dataset = read('data/input.data', index=':', format='runnerdata')\n",
    "\n",
    "rng = np.random.default_rng(42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparations: Queue setup\n",
    "\n",
    "Active Learning profits a lot from running calculations in the queue. Each stage usually runs multiple independent jobs ideally suited for parallel execution. `RunnerActiveLearn` can be executed with and without a queueing system, but submission is highly recommended.\n",
    "\n",
    "We define a dictionary containing some minimal settings for our queue:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "submit_settings = {\n",
    "    'queue_name': 'q8',\n",
    "    'num_cores': 1,\n",
    "    'output_name': 'slurm_outputs/slurm-%j.out'\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparations: Training setup\n",
    "\n",
    "The next step is to define training setups. `RunnerActiveLearn` takes a list of training setup dictionaries as an input argument and will train one HDNNP for each item in that list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "from runnerase.workflows import train_hdnnp\n",
    "from runnerase.utils.dicts import update\n",
    "from runnerase.utils.defaultoptions import DEFAULT_TRAINING_PARAMETERS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example, we plan to train three HDNNPs.\n",
    "\n",
    "Each training setup requires some mandatory keys:\n",
    "* `training_function`: A routine that can take a `Runner` calculator and train it until completion. Here, we use the `train_hdnnp` function of runnerase for our training. In principle, the user can freely define a custom training function.\n",
    "* `training_function_args` and `training_function_kwargs`: These parameters are passed on to the training function (none are needed in case of `train_hdnnp`).\n",
    "* `settings`: The specific training settings for each stage of training. This dictionary must contain keys like `mode1`, `mode2`, and `mode3`. The easiest way is to base it on the `DEFAULT_TRAINING_PARAMETERS` dictionary of runnerase and then customize it. This path was followed here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "training_setups = []\n",
    "for _ in range(3):\n",
    "\n",
    "    setup = {\n",
    "        'training_function': train_hdnnp,\n",
    "        'training_function_args': [],\n",
    "        'training_function_kwargs': {}\n",
    "    }\n",
    "\n",
    "    setup['settings'] = DEFAULT_TRAINING_PARAMETERS\n",
    "\n",
    "    update(setup, {\n",
    "        'settings': {\n",
    "            'mode2': {\n",
    "                'scale_symmetry_functions': True,\n",
    "                'epochs': 20\n",
    "            }\n",
    "        }\n",
    "    })\n",
    "\n",
    "    training_setups.append(setup)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By providing training setups in a list, you can train many HDNNPs with completely different parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Preparations: Simulation Setup\n",
    "\n",
    "In a very similar way, we continue to set up our simulations. Again, we provide one simulation setup dictionary for each simulation we want to run. Keep in that mind, that every simulation we define here will be executed with every potential.\n",
    "\n",
    "In this case, we define five very similar simulations, only differing in their initial structure. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "from runnerase.utils.atoms import get_elements\n",
    "from runnerase.workflows import run_nvt_simulation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "simulation_setups = []\n",
    "for i in range(5):\n",
    "\n",
    "    struc_idx = int(rng.integers(1, len(dataset)))\n",
    "    atoms = dataset[struc_idx]\n",
    "    elements = get_elements([atoms], sort_by_appearance=True)\n",
    "\n",
    "    simulation_setups.append({\n",
    "        'simulation_function': run_nvt_simulation,\n",
    "        'simulation_function_args': [],\n",
    "        'simulation_function_kwargs': {\n",
    "            'initial_structure': atoms,\n",
    "            'output_freq': 1,\n",
    "            'elements': elements\n",
    "        }\n",
    "    })"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The arguments are very similar to what we saw before. We need to define a `simulation_function` including `args` and `kwargs`. Above, we pass the `initial_structure`, `output_freq` and `elements` mapping to the simulation function. The `elements` are very important: it happens quite often, that the structure we run the simulations for do not contain all the elements we trained on. In such a case, we should always define the `elements` mapping between LAMMPS and the ML-HDNNP package ourselves."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Running one active learning cycle\n",
    "\n",
    "All required setup is done. Hopefully you could get a feeling for the flexibilty that this active learning class gives you. We go on to initialize the calculator itself by passing it all the components we defined above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "ral = RunnerActiveLearn(\n",
    "    directory='cycle1',\n",
    "    dataset=dataset,\n",
    "    simulation_setups=simulation_setups,\n",
    "    rng=rng,\n",
    "    runner_cmd='runner1 > PREFIX.out',\n",
    "    submission_settings=submit_settings,\n",
    "    training_setups=training_setups,\n",
    "    submit=True,\n",
    "    write_log=True\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Apart from the keywords that have already been explained, we define\n",
    "* `directory`: parent directory for this active learning cycle. It will contain all potentials, simulations, recalculations and analysis results.\n",
    "* `submit`: whether to submit the calculations to the queue or not.\n",
    "* `write_log`: whether to write a log file (can be quite helpful and stays pretty small)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`RunnerActiveLearn` gives you full control about what you want to do. For our first steps, we run the full cycle automatically using `run_cycle`. This will run all the steps listed above and submit the individual jobs to the queue. Each step will wait for the previous one to finish."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ral.run_cycle()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Take a look at the directory structure that `run_cycle` creates for you: it starts by generating the parent folder `cycle1`, containing one folder `pot_1` to `pot_3` for each potential we are training.\n",
    "\n",
    "Inside each potential folder, it generates a `simulation` folder containing all the simlulation jobs that run for this potential.\n",
    "\n",
    "One final level of nesting: inside each folder of a simulation, a `recalculation` folder is created. This contains the RuNNer Mode 3 runs for the recalculation of the simulation trajectory with all existing potentials."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Evaluating the results\n",
    "\n",
    "After the training cycle has finished, we can take a look at the results. `run_cycle` will analyze all recalculations for you and create comparison plots of energies and forces between the different neural networks. It will also step through the simulations and record all extrapolating structures into an xyz file.\n",
    "\n",
    "Here are some examples of the plots it creates:\n",
    "\n",
    "![Comparison Energy mean vs prediction](../resources/active_learn_results/comparison_energy_mean_vs_prediction.png)\n",
    "![Comparison Energy error mean vs prediction](../resources/active_learn_results/comparison_energy_error_mean_vs_prediction.png)\n",
    "![Comparison Forces mean vs prediction](../resources/active_learn_results/comparison_forces_mean_vs_prediction.png)\n",
    "![Comparison Forces error mean vs prediction](../resources/active_learn_results/comparison_forces_error_mean_vs_prediction.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, the statistics underlying this plot are saved to a pickled file for further analysis."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By default, an XYZ file of the all the extrapolating structures along a trajectory is exported. However, we can go even further and automatically extract the atomic environments around an extrapolating atom. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "from runnerase.utils.submit import slurm_barrier\n",
    "\n",
    "ral = RunnerActiveLearn(\n",
    "    directory='cycle1',\n",
    "    dataset=dataset,\n",
    "    simulation_setups=simulation_setups,\n",
    "    rng=rng,\n",
    "    runner_cmd='runner1 > PREFIX.out',\n",
    "    submission_settings=submit_settings,\n",
    "    potential_paths=[f'cycle1/pot_{i}' for i in range(3)],\n",
    "    submit=True,\n",
    "    write_log=True\n",
    ")\n",
    "\n",
    "ral.load_recalculated()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "sim = ral.potentials[0].simulations[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sim.write_extrapolating_structures(\n",
    "    filename='environments.xyz',\n",
    "    whole_structure=False,\n",
    "    margin=0.0\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This code will produce a trajectory similar to this (only a selection of structures is shown here):\n",
    "\n",
    "<img src=\"../resources/active_learn_results/environments.gif\" width=\"750\" align=\"center\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the `margin` keyword is given, a cubic box around the circular environments is cut out of the trajectory as well. We can use this in a LAMMPS simulation to equilibrate the periodic boundaries again. For example:\n",
    "\n",
    "![Single environment with margin](../resources/active_learn_results/environment_with_margin.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Rerun individual steps\n",
    "\n",
    "Of course, it is also possible to start the active learning process from in between any of these steps by loading the previous one into memory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "Mode 3: No. Structures:  94%|█████████▍| 16/17 [00:01<00:00, 15.10it/s]\n",
      "Mode 3: No. Structures:  94%|█████████▍| 16/17 [00:01<00:00, 14.99it/s]\n",
      "Mode 3: No. Structures:  94%|█████████▍| 16/17 [00:01<00:00, 15.02it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 15.02it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 14.97it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 15.00it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 13.82it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 13.75it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 13.68it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 13.65it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 13.61it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 13.64it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 13.54it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 13.51it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 13.52it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 19/19 [00:01<00:00, 14.96it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 19/19 [00:01<00:00, 14.94it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 19/19 [00:01<00:00, 14.90it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 16/16 [00:01<00:00, 14.27it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 16/16 [00:01<00:00, 14.26it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 16/16 [00:01<00:00, 14.17it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 16/16 [00:01<00:00, 13.86it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 16/16 [00:01<00:00, 13.85it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 16/16 [00:01<00:00, 13.77it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 16/16 [00:01<00:00, 13.57it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 16/16 [00:01<00:00, 13.46it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 16/16 [00:01<00:00, 13.49it/s]\n",
      "Mode 3: No. Structures:  95%|█████████▍| 18/19 [00:01<00:00, 14.38it/s]\n",
      "Mode 3: No. Structures:  95%|█████████▍| 18/19 [00:01<00:00, 14.36it/s]\n",
      "Mode 3: No. Structures:  95%|█████████▍| 18/19 [00:01<00:00, 14.36it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 14.05it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 14.00it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 14.05it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 14.10it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 14.17it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 14.20it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 13.17it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 13.74it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 17/17 [00:01<00:00, 13.78it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 13.56it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 13.62it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 13.54it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 13.45it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 13.40it/s]\n",
      "Mode 3: No. Structures: 100%|██████████| 18/18 [00:01<00:00, 13.43it/s]\n"
     ]
    }
   ],
   "source": [
    "ral.load_simulations()\n",
    "job_ids = ral.recalculate_trajectories()\n",
    "\n",
    "slurm_barrier(job_ids)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "ral.load_recalculated()\n",
    "job_ids = ral.analyze_results(plot=True)\n",
    "\n",
    "slurm_barrier(job_ids)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "runnerase-demo",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
